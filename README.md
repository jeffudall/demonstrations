# Hello and welcome! #

### What is this repository for? ###
This repository was created to showcase code I've written to prospective employers. 

### What can I do now that I'm here? ###
Depending on your skill level and interest, you can choose from the following options .

* Click on the Source link on the left and navigate to the project to view the source directly.
* Clone or Fork the code and load it up in your favorite code editor to view.
* Visit my [website](http://www.jeffudall.com/demos/) to see a demo of the project and code in action.