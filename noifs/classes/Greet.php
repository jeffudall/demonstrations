<?php

class Greet
    {
        protected $_greeting;
    
        public function setgreeting($string)
            {
                $this->_greeting = $string;
            }
    
        public function getgreeting()
            {
                return $this->_greeting;
            }
    }


class French extends Greet
    {
        function __construct()
            {
                $this->_greeting = "Bon jour!";
            }
    }

class German extends Greet
    {
        function __construct()
            {
                $this->_greeting = "Guten Tag!";
            }
    }

class Australian extends Greet
    {
        function __construct()
            {
                $this->_greeting = "G'day!";
            }
    }

class Russian extends Greet
    {
        function __construct()
            {
                $this->_greeting = "Privyet!";
            }
    }

class Choose extends Greet
    {
        function __construct()
            {
                exit();
            }
    }
