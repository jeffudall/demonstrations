<?php
class Page {

    private $_header;   // Holds the heredoc string for the header HTML
    private $_footer;   // Holds the heredoc string for the footer HTML
    private $_title;    // Holds string for the title of the page
    private $_message;  // Holds the String for the message to be displayed
    private $_msgdiv;   // Holds the heredoc string for the message div HTML
    private $_formstate;    // Boolean true if form has been submitted, otherwise false.
    
    // On object creation, provide a title of the page to be set in the $_title property
    function __construct($string) {
        $this->_title = $string;    // The HTLM <title> of the page
        $this->_formstate = isset($_POST['submit']);    // Set $_formstate property to true if submit button was clicked, otherwise false.
    }
    
    // Return the value of $_formstate
    private function getformstate() {
        return $this->_formstate;
    }
    
    
    // Creates the webpage from the <doctype> to opening <body> tag
    public function getheader() {
        $this->_header = <<< HEADER
<!doctype html>
<html class="no-js" lang="en">
    <head>
        <meta charset="utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0" />
        <title>{$this->_title}</title>
        <link rel="stylesheet" href="css/app.css" />
        <script src="bower_components/modernizr/modernizr.js"></script>
    </head>
  <body>
HEADER;
        echo $this->_header;
    }

    // Creates the form for the page
    public function getform() {
        $this->_form = <<< FORM
        <div class="row">
            <div class="large-12 columns">
                <h2>Choose a country and get greeted in that language!</h2>
                <form action="" method="post">
                    <label>Available Countries:
                        <select name="lang">
                            <option value="choose">Choose...</option>
                            <option value="french">France</option>
                            <option value="german">Germany</option>
                            <option value="australian">Australia</option>
                            <option value="russian">Russia</option>
                        </select>
                    </label>
            
                    <input type="submit" name="submit" value="Submit" class="button">
                </form>
            </div>
        </div>
FORM;
        echo $this->_form;
    }
    
    // Check if the form has been submitted. If it has, create a new $lang object of the class: $_POST['lang'], otherwise do nothing.
    // Also, this suppresses the "no key for 'lang'" error on first page load
    public function getmessage(Greet $lang) {
        if ($this->getformstate()) $lang = new $_POST['lang'];
        
        $this->_message = $lang->getgreeting();
        $this->_msgdiv = <<< MSG
        <div class="row">
            <div class="large-12 columns">
                <h2>{$this->_message}</h2>
            </div>
        </div>
MSG;
        echo $this->_msgdiv;
    }
    
    // Creates the <script>, closing <body> and <html> tags of the webpage, effectively finishing the page.
    public function getfooter() {
        $this->_footer = <<< FOOTER
        <script src="bower_components/jquery/dist/jquery.min.js"></script>
        <script src="bower_components/foundation/js/foundation.min.js"></script>
        <script src="js/app.js"></script>
    </body>
</html>
FOOTER;
        echo $this->_footer;
    }

}   //End of Page class


    





    
