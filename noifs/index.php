<?php
// The code in this file basically creates the index.php page by calling in all
// of the separate, modular pieces.


// Require the class autoloader. We don't have to require any specific files from the classes folder.
// The autoloader does so for us when needed.
require_once('init.php');

// Create an instance of a Page and pass it the title of the page
$index = new Page('Greet | Index');

// Create a new Greet object to pass to the getmessage() function. The Greet object contains all of the
// greeting messages.
$speak = new Greet;

// Create the header html of the page
$index->getheader();

// Create the form html of the page
$index->getform();

// Create the html for the greeting message. Also, displays the message in the language
// based on what was chosen from the select menu.
$index->getmessage($speak);

// Create the html for the footer of the page
$index->getfooter();


?>














