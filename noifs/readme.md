# noifs PHP application #

This noifs PHP application is a small application I wrote using object oriented PHP. The goal was to make logical choices without using if statement logic. This was achieved by using classes
and constructors for those classes. When a choice is made in the select menu, the selection instantiates a class that contains a property of the greeting for that language. Then it's just a
matter of using a method of the class to output the value of the property. The only if logic used in this application is to determine if the there has been a POST event for loading the page. 