//
//  Class auto loader. This keeps you from having to individually load multiple classes
// on every page.

<?php

spl_autoload_register(function($class)
    {
        require_once('classes/' . $class . '.php');
    });
